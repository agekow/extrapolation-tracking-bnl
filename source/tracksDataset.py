import numpy as np
import pandas as pd
import pickle
import argparse 


class Hit:
    def __init__(self, volume_id, layer_id, x,y,z):
        self.volume_id = volume_id
        self.layer_id = layer_id
        self.x = x
        self.y = y
        self.z = z

class Track:
    def __init__(self, hits):
        self.hits = hits


class TrackLoader:

    def __init__(self, filePath):
        with open(filePath,'rb') as pFile:
            _ = pickle.load(pFile)
            _ = pickle.load(pFile)
            _ = pickle.load(pFile)
            _ = pickle.load(pFile)
            x_train = pickle.load(pFile,  encoding='latin1')
            _ = pickle.load(pFile)
            x_train_layer = pickle.load(pFile,  encoding='latin1')

        self.tracks = x_train
        self.targets = None
        self.layerInfo = x_train_layer

        self.xNorm = 1068
        self.yNorm = 1068
        self.zNorm = 3025.5

    # NEED TO CHANGE THIS TO INCLUDE ENDCAPS WHEN DESIRED
    def detectorInfo_to_featureVector(self, detectorInfoLoc, cols, nhits):
        track = []
        for i in range(nhits):
            # first 3 spot is barrel encoding (vols 17,24,29). 
            # Next 8 are One-hot-encoded layer id
            zeros = np.zeros((11,)) #can add one more to hold a boolean for isBarrel. This should be removed when making the input data
            vol_id = detectorInfoLoc[cols[i]]
            pixOrSct = detectorInfoLoc[cols[i+30]]
            layer_idx = detectorInfoLoc[cols[i+15]]
            if vol_id == 17:    
                zeros[0:3] = np.array([1,0,0])
                #zeros[-1] = 1
            elif vol_id == 24:  
                zeros[0:3] = np.array([0,1,0])
                #zeros[-1] = 1
            elif vol_id == 29: 
                zeros[0:3] = np.array([0,0,1])
                #zeros[-1] = 1
            else:
                zeros[0:3] = 2 # CHANGE THIS. USED AS FLAG TO REMOVE ENDCAP HITS FOR NOW
            #zeros[0] = vol_id
            #zeros[1] = pixOrSct
            if layer_idx !=0: zeros[2+int(layer_idx/2)] = 1 #layer id are even only, convert to consecutive integers. Total of 8 layers (including endcaps)
            track.append(zeros)
        return np.asarray(track)

    def dataWindow(self, window_size, n_features, normalize=True, barrelOnly=False, endcapOnly=False):
        #Input (tracks) should be an array of shape (n_samples, n_hits*n_features where the features are listed as x0, y0, z0, x1, y2, z2, ...)
        # returns an x array of size (n_samples, window_size, n_features)
        # returns a y array of size (n_samples, n_features)
        if (barrelOnly and endcapOnly): raise Exception("tracks cannot be barrel only and endcap only")

        seeds = []
        targets = []
        cols = self.layerInfo.columns
        #for i in range(1):

        tot = 0
        for i in range(self.tracks.shape[0]):

            track = self.tracks[i]
            x=track[0:15]
            y=track[15:30] 
            z=track[30:]
            dzdr, z0 = np.polyfit(z, np.sqrt(y**2+x**2),1)
            if abs(z0) > 150: continue
            tot+=1
            detectorID = self.layerInfo.iloc[i]
            new_track = None
            if normalize:
                x= x / self.xNorm
                y= y / self.yNorm
                z= z / self.zNorm
            nHits = np.where(track==0)[0] 
            if not np.any(nHits): nHits = 15
            else: nHits = nHits[0]
            
            # use only a single hit per layer
            detectorFeatures = self.detectorInfo_to_featureVector(detectorID, cols, nHits)
            detector = np.array([])
            indices = []
            for j in range(nHits):
                currentDetector = detectorFeatures[j]
                if np.array_equal(detector, currentDetector): continue
                detector = currentDetector
                indices.append(j)
            nHits = len(indices)
            new_track = np.append(x[indices].reshape(nHits,1), y[indices].reshape(nHits,1),axis=1)
            new_track = np.append(new_track, z[indices].reshape(nHits,1), axis=1)
            new_track = np.append(new_track, detectorFeatures[indices], axis=1)

            for w in range(nHits - window_size):

                seed = new_track[w:w+window_size].reshape(1,window_size,n_features)
                target = new_track[w+window_size].reshape(n_features)
                
                #CHANGE THESE CONDITIONALS. RIGHT NOW THIS ONLY ACCOMODATES BARREL ONLY
                if (barrelOnly and ((np.any(seed[:,:,3] == 2)) or (np.any(target[3]==2)))): break
                if (endcapOnly and ((np.any(seed[:,:,3] == 1)) or (np.any(target[3]==1)))): break
                seeds.append(seed)
                targets.append(target)

            if i%100000 == 0: print(i, " tracks done")
        seeds = np.asarray(seeds)
        targets = np.asarray(targets)
        
        self.tracks = seeds.reshape(-1,window_size,n_features) 
        self.targets = targets.reshape(-1,n_features)
        print("total tracks done: ", tot)

    def cartesian_to_cylindrical(self):
        xPhi = np.arctan2(self.tracks[:,:,1].flatten(), self.tracks[:,:,0].flatten()) / np.pi
        xPhi = xPhi.reshape(-1,3,1)
        yPhi = np.arctan2(self.targets[:,1].flatten(), self.targets[:,0].flatten()) / np.pi
        yPhi = yPhi.reshape(-1,1)

        xRho = np.sqrt((self.tracks[:,:,1].flatten())**2 + (self.tracks[:,:,0].flatten())**2 ) / 1068
        xRho = xRho.reshape(-1,3,1)
        yRho = np.sqrt((self.targets[:,1].flatten())**2 + (self.targets[:,0].flatten())**2+(Y[:,2].flatten())**2) / 1068
        yRho = yRho.reshape(-1,1)

        #(rho, z, phi)
        X = np.append(xRho, self.tracks[:,:,2].reshape(-1,3,1) / 3025.5, axis=2)
        X = np.append(X, xPhi, axis=2)
        Y = np.append(yRho, self.targets[:,2].reshape(-1,1) / 3025.5, axis=1)
        Y = np.append(Y, yPhi, axis=1)

        self.tracks = X
        self.targets = Y

    def getData(self, window_size, n_features, normalize=True, barrelOnly=False, endcapOnly=False, cylindrical=False):
        self.dataWindow(window_size, n_features,normalize, barrelOnly, endcapOnly)
        if cylindrical:
            self.cartesian_to_cylindrical(cylindrical=True)
            self.getData()
   
        return self.tracks, self.targets[:,:3]

def main():
    import os
    parser = argparse.ArgumentParser()

    parser.add_argument("--dataPath", type=str, default='/usatlas/u/agekow/trackingPickles/formatted/', help="file path to pickle data")
    parser.add_argument("--windowSize", type=int, default=3, help="window size of algorithm")
    parser.add_argument("--cylindrical", action='store_true', help="use cylindrical coordinates")
    parser.add_argument("--useLayers", action='store_true', default=True, help="use one hot encoded layer indices during training") #default can be reset to false
    parser.add_argument("--barrelOnly", action='store_true', default=False, help="use only track segments in the barrel")
    parser.add_argument("--endcapOnly", action='store_true', default=False, help="use only track segments in the endcap")
    parser.add_argument("--outFile", type=str, help="name of output file")
    args = parser.parse_args()

    

    if os.path.isfile(args.dataPath):
        dataLoader = TrackLoader(args.dataPath)
        X, Y = dataLoader.getData(args.windowSize, 14, True, args.barrelOnly, args.endcapOnly)
    elif os.path.isdir(args.dataPath):
        X = None
        Y = None
        for trainingData in args.dataPath:
            dataLoader = TrackLoader(os.path.join(args.dataPath, trainingData))
            x, y = dataLoader.getData(args.windowSize, 14, True, args.barrelOnly, args.endcapOnly)

            if X is None:
                X = x
                Y = y
            else:
                X = np.append(X, axis=0)
                Y = np.append(Y, axis=0)

    #np.savez("/hpcgpfs01/scratch/sabidi/ExtrapolationPickles/"+args.outFile, inputs=X, targets=Y)
    np.savez("/usatlas/u/agekow/trackingPickles/formatted/" + args.outFile, inputs=X, targets=Y)
if __name__ == "__main__":
    main()

