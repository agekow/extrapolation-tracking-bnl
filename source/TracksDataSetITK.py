import numpy as np
import pandas as pd
import pickle
import argparse 
import matplotlib.pyplot as plt
import tensorflow as tf

#compile all the info into single objects
class Hit:
    def __init__(self, x,y,z,volID,layerID):
        self.x = x
        self.y = y
        self.z = z
        self.rho = np.sqrt(x**2+y**2)
        self.volumeID = volID
        self.layerID = layerID
        self.oheVol = None
        self.oheLayer = None

    def OneHotEncodeVolume(self, hit):
        #Use the index of the sorted volumeIDs as one-hot-encoding position
        hit.oheVol = tf.keras.utils.to_categorical(self.volumeMap[hit.volumeID])
            
        
    def OneHotEncodeLayer(self, hit):
        # Layers are incrimented in units of 2
        hit.oheLayer = tf.keras.utils.to_categorical(hit.layerID/2) 

class Track:
    def __init__(self, hits, detectorInfo=None):
        self.hits = hits
        self.x = np.array([hit.x for hit in self.hits])
        self.y = np.array([hit.y for hit in self.hits])
        self.z = np.array([hit.z for hit in self.hits])
        self.oheVol = np.array([hit.oheVol for hit in self.hits])
        self.oheLayer = np.array([hit.oheLayer for hit in self.hits])
        self.rho = np.sqrt(self.x**2 + self.y**2)
        self.theta = np.mean(np.arccos(self.z/np.sqrt(self.x**2+self.y**2+self.z**2)))
        self.eta = -np.log(np.tan(self.theta/2))
        self.layerID = [hit.layerID for hit in self.hits]
        self.volumeID = [hit.layerID for hit in self.hits]
        self.z0 = None
        self.oheLayer=None
        self.oheVol=None
        self.phi = None
        arr=np.array([])
        for x,y,z in zip(self.x, self.y, self.z):
           arr = np.append(arr,np.array([x,y,z]))
        self.hitArr = arr.reshape(-1,3)
    
        self._index = 0
    
    def __len__(self):
        return len(self.hits) 

    def __iter__(self):
        return self

    def __next__(self):
        if self._index < len(self.hits):
            hit = self.hits[self._index]
            self._index +=1
            return hit
        raise StopIteration

    def setZ0(self, z0=None):
        if z0 is None:
            dzdr, z0 = np.polyfit(self.z, np.sqrt(self.x**2+self.y**2),1)
        else self.z0 = z0

    def getZ0(self):
        return self.z0
   
    def plotTrackXY(self):
        plt.scatter(self.x, self.y)
        plt.xlabel("x")
        plt.ylabel("y")
        
    def plotTrackZRho(self):
        plt.scatter(self.z, self.rho)
        plt.xlabel("z")
        plt.ylabel(r"$\rho$")
    
    def OneHotEncodeVolume(self, volumeMap):
        #Use the index of the sorted volumeIDs as one-hot-encoding position
        allOHE = np.array([])
        for hit in self.hits:
            ohe = tf.keras.utils.to_categorical(volumeMap[hit.volumeID], num_classes=15)
            allOHE = np.append(allOHE, ohe)
        self.oheVol = allOHE.reshape(len(self.hits),-1)

    def OneHotEncodeLayer(self):
        # Layers are incrimented in units of 2
        allOHE = np.array([])
        for hit in self.hits:
            ohe = tf.keras.utils.to_categorical(hit.layerID/2, num_classes=30)
            allOHE = np.append(allOHE, ohe)
        self.oheLayer = allOHE.reshape(len(self.hits),-1)
        

    def SetOneHotEncoding(self, volumeMap):
        self.OneHotEncodeLayer()
        self.OneHotEncodeVolume(volumeMap)
        self.hitArr = np.hstack([self.hitArr, self.oheVol])
        self.hitArr = np.hstack([self.hitArr, self.oheLayer])

    def SingleHitPerLayerTrack(self):
        keepHits = []
        for idx,hit in enumerate(self.hits):
            
            if idx==0:
                keepHits.append(hit)
                prevVol = hit.volumeID
                prevLayer = hit.layerID
            else:
                if hit.volumeID ==  prevVol:
                    if ( (hit.layerID == prevLayer+2) or (hit.layerID == prevLayer-2) ):
                        keepHits.append(hit)
                        prevLayer = hit.layerID
                    else:
                        continue
                else:
                    keepHits.append(hit)
                    prevLayer = hit.layerID
                    prevVol = hit.volumeID
        
        return Track(keepHits)

    def SetPhi(self):
        self.phi = np.arctan2(self.hitArr[:,1], self.hitArr[:,0])

    def Cartesian_To_Cylindrical(self):
        # rho, phi, z
        if self.phi is None: self.SetPhi()
        self.hitArr[:,0] = self.rho
        self.hitArr[:,1] = self.phi
        
class TrackLoader:

    def __init__(self, filePath):
        with open(filePath,'rb') as fp:
            print("opening file")
            _ = pickle.load(fp, encoding='latin1')
            _ = pickle.load(fp, encoding='latin1')
            _ = pickle.load(fp, encoding='latin1')
            _ = pickle.load(fp, encoding='latin1') 
            x_train = pickle.load(fp, encoding='latin1') #x_trainFlat
            x_test = pickle.load(fp, encoding='latin1') #x_testFlat
            _ = pickle.load(fp, encoding='latin1') #y_train
            _ = pickle.load(fp, encoding='latin1') #y_test
            _ = pickle.load(fp, encoding='latin1') #weight_train
            _ = pickle.load(fp, encoding='latin1') #weight_test
            _ = pickle.load(fp, encoding='latin1') #y_train_old
            _ = pickle.load(fp, encoding='latin1') #y_test_old
            x_trainlayer = pickle.load(fp, encoding='latin1') # volume and layer id
            x_testLayer = pickle.load(fp, encoding='latin1') # volume and layer id

        if not len(x_test) == 0:
            inpData = np.vstack((x_train, x_test))
            detectorData = pd.concat((x_trainlayer, x_testLayer))
        else:
            inpData = x_train
            detectorData = x_trainlayer

        for i in range(len(inpData)):
            hits=[]
            track = inpData[i]
            for j in range(15):
                if track[j] != 0: #If the coordinate is not 0 create a hit
                    volID = detectorData.iloc[i][j]
                    layerID = detectorData.iloc[i][j+15]
                    hit = Hit(track[j],track[j+15],track[j+30],volID,layerID)
                    hits.append(hit)
            self.tracks.append(Track(hits))

        self.targets = None

        self.xNorm = 1014
        self.yNorm = 1014
        self.zNorm = 3480

        self.barrelVolumes = (9,16,23)
        self.endcapVolumes = (8,10,13,14,15,18,19,20,22,24,2,25)
        self.volLayerMap = {9: {2, 4},
            16: {2, 4, 6},
            23: {2, 4, 6, 8},
            8: {2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58},
            10: {2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58},
            13: {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34},
            14: {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32},
            15: {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36},
            18: {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34},
            19: {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32},
            20: {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36},
            22: {2, 4, 6, 8, 10, 12},
            24: {2, 4, 6, 8, 10, 12},
            2: {2, 4},
            25: {2, 4}}


        self.volumes = list(self.barrelVolumes + self.endcapVolumes)
        self.volumes.sort()
        self.volumeMap = {}
        for idx,volID in enumerate(self.volumes):
            self.volumeMap[volID]=idx

    # create a list of tracks in their hit representation
    def makeTracks(self, detectorData):
        hits=[]
        for i in range(len(self.inpData)):
            track = self.inpData[i]
            for j in range(15):
                if track[j] != 0: #If the coordinate is not 0 create a hit
                    volID = detectorData.iloc[i][j]
                    layerID = detectorData.iloc[i][j+15]
                    hit = Hit(track[j],track[j+15],track[j+30],volID,layerID)
                    hits.append(hit)
        self.tracks.append(Track(hits))
    

    def OneHotEncodeVolume(self, volumeID):
        return tf.keras.utils.to_categorical(self.volumeMap[hit.volumeID])
        
    def OneHotEncodeLayer(self, layerID):
        # Layers are incrimented in units of 2
        return tf.keras.utils.to_categorical(layerID/2) 

    def _normalizeCartesian(self):
        self.tracks[:,:,0] /= self.xNorm
        self.tracks[:,:,1] /= self.yNorm
        self.tracks[:,:,2] /= self.zNorm

        self.targets[:,0] /= self.xNorm
        self.targets[:,1] /= self.yNorm
        self.targets[:,2] /= self.zNorm


    def _normalizeCylindrical(self):
        self.tracks[:,:,0] /= self.xNorm #rho
        self.tracks[:,:,1] /= np.pi      #phi
        self.tracks[:,:,2] /= self.zNorm #z

        self.targets[:,0] /= self.xNorm
        self.targets[:,1] /= np.pi
        self.targets[:,2] /= self.zNorm


    def dataWindow(self, window_size, onehotencode=False, normalize=True, barrelOnly=False, 
                    endcapOnly=False, cylindrical=False, nTracks=-1, classify=False):
        #Input (tracks) should be an array of shape (n_samples, n_hits*n_features where the features are listed as x0, y0, z0, x1, y2, z2, ...)
        # returns an x array of size (n_samples, window_size, n_features)
        # returns a y array of size (n_samples, n_features)
        if (barrelOnly and endcapOnly): raise Exception("tracks cannot be barrel only and endcap only")

        seeds = []
        targets = []
        tot = 0
        counter = 0
        if nTracks < 0: nTracks = len(self.tracks)
        while tot < nTracks:
            if counter >=nTracks: break
            track = self.tracks[counter]
            track.setZ0()
            counter+=1
            if abs(track.getZ0()) > 150: 
                continue     
            # if abs(track.eta) > 0.1:
            #     continue      
            #use only a single hit per layer
            newTrack = track.SingleHitPerLayerTrack()
            if onehotencode: newTrack.SetOneHotEncoding(self.volumeMap)
            if cylindrical: newTrack.Cartesian_To_Cylindrical()
            if len(newTrack) < window_size: 
                continue
            for w in range(len(newTrack) - window_size):
                seed = newTrack.hitArr[w:w+window_size].reshape(1,window_size,-1)
                seedVolumes = [hit.volumeID for hit in newTrack.hits[w:w+window_size]] 
                target = newTrack.hitArr[w+window_size]
                targetVolume = newTrack.hits[w+window_size].volumeID
                targetLayer = newTrack.hits[w+window_size].layerID
                if (barrelOnly and (targetVolume in self.endcapVolumes)): 
                    break
                if (endcapOnly and (((np.any(seedVolumes) in self.barrelVolumes)) or (targetVolume in self.barrelVolumes))): 
                    break
            
                seeds.append(seed)
                if classify:
                    oheVol = self.OneHotEncodeVolume(targetVolume)
                    oheLayer = self.OneHotEncodeLayer(targetLayer)
                    targets.append(target[3:])
                    nOutputFeatures = 45
                else:
                    targets.append(target[0:3])
                    n_outputFeatures = 3
            tot+=1 
            if counter%100000 == 0: print(counter, " tracks done")
        seeds = np.asarray(seeds)
        seeds = np.squeeze(seeds,1)
        targets = np.asarray(targets)
        print("seed shape ",seeds.shape )
        n_features = seeds.shape[2] #check that this is the correct index
        self.tracks = seeds.reshape(-1,window_size,n_features) 
        self.targets = targets.reshape(-1,n_outputFeatures)
        if normalize: 
            if not cylindrical: self._normalizeCartesian()
            else: self._normalizeCylindrical()

    def getData(self, window_size, onehotencode=False, normalize=True, barrelOnly=False, 
                    endcapOnly=False, cylindrical=False, nTracks=-1, classify=False):

        self.dataWindow(window_size, onehotencode, normalize, barrelOnly, endcapOnly, cylindrical, nTracks, classify)
        print("total training samples:", len(self.tracks))
        return self.tracks, self.targets[:,:3]

def main():
    import os
    parser = argparse.ArgumentParser()

    parser.add_argument("--dataPath", type=str, default='/usatlas/u/agekow/trackingPickles/formatted/', help="file path to pickle data")
    parser.add_argument("--windowSize", type=int, default=3, help="window size of algorithm")
    parser.add_argument("--cylindrical", action='store_true', help="use cylindrical coordinates")
    parser.add_argument("--onehotencode", action='store_true', default=False, help="use one hot encoded layer indices during training") #default can be reset to false
    parser.add_argument("--barrelOnly", action='store_true', default=False, help="use only track segments in the barrel")
    parser.add_argument("--endcapOnly", action='store_true', default=False, help="use only track segments in the endcap")
    parser.add_argument("--nTracks", type=int, default=-1, help="use only track segments in the endcap")
    parser.add_argument("--classify", action='store_true', default=False, help="targets are the volume and layeres one hot encoded")


    parser.add_argument("--outFile", type=str, help="name of output file")
    args = parser.parse_args()
    
    if os.path.isfile(args.dataPath):
        dataLoader = TrackLoader(args.dataPath)
        X, Y = dataLoader.getData(window_size=args.windowSize, onehotencode=args.onehotencode,
                            normalize=True, barrelOnly=args.barrelOnly, endcapOnly=args.endcapOnly,
                            cylindrical=args.cylindrical, nTracks = args.nTracks, classify=args.classify)

        print(X.shape, Y.shape)

    elif os.path.isdir(args.dataPath):
        X = None
        Y = None
        for trainingData in args.dataPath:
            dataLoader = TrackLoader(os.path.join(args.dataPath, trainingData))
            x, y = dataLoader.getData(args.windowSize, args.onehotencode, True, args.barrelOnly, args.endcapOnly, False, -1)

            if X is None:
                X = x
                Y = y
            else:
                X = np.append(X, axis=0)
                Y = np.append(Y, axis=0)

    np.savez("../trainingData/"+args.outFile,inputs=X,targets=Y)
    #np.savez("/hpcgpfs01/scratch/sabidi/ExtrapolationPickles/"+args.outFile, inputs=X, targets=Y)
    #np.savez("/usatlas/u/agekow/trackingPickles/formatted/" + args.outFile, inputs=X, targets=Y)
if __name__ == "__main__":
    main()

