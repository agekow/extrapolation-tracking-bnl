from grpc import ChannelConnectivity
import tensorflow as tf
import tensorflow_addons as tfa

def create_optimizer(trial):
    # We optimize the choice of optimizers as well as their parameters.

    optimizer_name = trial.suggest_categorical("optimizer", ["Adam", "sgd"])
    if optimizer_name == "Adam":
        adam_lr = trial.suggest_float("adam_lr", 1e-5, 1e-1, log=True)
        return tf.keras.optimizers.Adam(learning_rate=adam_lr)
    else:
        sgd_lr = trial.suggest_float("sgd_lr", 1e-5, 1e-1, log=True)
        sgd_momentum = trial.suggest_float("sgd_momentum", 1e-5, 1e-1, log=True)
        return tf.keras.optimizers.SGD(learning_rate=sgd_lr, momentum=sgd_momentum)

def esn(trial):
    n_layers = trial.suggest_int("n_layers", 1, 3)
    connectivity = trial.suggest_float("connectivity", .1, .9, log=True)
    model = tf.keras.models.Sequential()
    for i in range(n_layers):
        num_hidden = trial.suggest_int(f'n_units_l{i}', 100, 1000, log=True)
        if i == n_layers-1: return_sequence = False
        else: return_sequence = True
        model.add(tfa.layers.ESN(units=num_hidden, connectivity=connectivity, return_sequences=return_sequence))
    model.add(tf.keras.layers.Dense(3, activation='tanh'))

    optimizer = create_optimizer(trial)

    model.compile(
        loss='mse',
        optimizer=optimizer,
        metrics=["mse"]
    )
    return model
