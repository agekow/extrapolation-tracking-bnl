import tensorflow as tf
from tensorflow.keras.layers import Dense, GRU, Input, LayerNormalization
from tensorflow.keras.models import Sequential
from lossFunctions import angleMSE
import optuna



def format_data_rnn(data, n_features=3):
    #takes an array of size (n_samples, n_hits*n_features where the features are listed as x0, y0, z0, x1, y2, z2, ...)
    #and reshapes them into size (n_samples, n_hits, n_features)
    batch_size = data.shape[0]
    n_hits = int(data.shape[1] / n_features)
    return data.reshape(batch_size, n_hits, n_features)

def create_optimizer(trial):
    # We optimize the choice of optimizers as well as their parameters.

    optimizer_name = trial.suggest_categorical("optimizer", ["Adam", "RMSprop"])
    if optimizer_name == "Adam":
        adam_lr = trial.suggest_float("adam_lr", 1e-5, 1e-1, log=True)
        return tf.keras.optimizers.Adam(learning_rate=adam_lr)
    else:
        rmsprop_lr = trial.suggest_float("rmsprop_lr", 1e-5, 1e-1, log=True)
        rmsprop_momentum = trial.suggest_float("rmsprop", 1e-5, 1e-1, log=True)
        return tf.keras.optimizers.RMSprop(learning_rate=rmsprop_lr, momentum=rmsprop_momentum)

def rnn(trial):
    n_layers = trial.suggest_int("n_layers", 1, 3)

    model = Sequential()
    for i in range(n_layers):
        num_hidden = trial.suggest_int(f'n_units_l{i}', 8, 64, log=True)
        if i == n_layers-1: return_sequence = False
        else: return_sequence = True
        model.add(GRU(units=num_hidden, return_sequences=return_sequence))
    model.add(Dense(3, activation='tanh'))

    optimizer = create_optimizer(trial)

    model.compile(
        loss='mse',
        optimizer=optimizer,
        metrics=["mse"]
    )
    return model
