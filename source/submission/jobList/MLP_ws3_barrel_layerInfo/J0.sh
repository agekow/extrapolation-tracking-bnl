#!/bin/bash

python /usatlas/u/agekow/usatlasdata/trackExtrapolation_training/windowTraining.py  --dataPath /usatlas/u/agekow/usatlasdata/alexTrackNNAnalysis/run/PickleOutput/processed/pickles/test.root_0.pkl --epochs 2000 --batch 1024 --windowSize 3 --modelType MLP --barrelOnly --useLayers
