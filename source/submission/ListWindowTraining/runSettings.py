def getACTSRunSettings():
    runTypes = []
    # All the runs we want to do
    runDict = {}
    runDict["baseName"]     = ""
    runDict["epochs"]       = "10"
    runDict["batch"]        = "1024"
    runDict["retrain"]     = ""
    
    runTypes.append(runDict)

    return runTypes
