def getMaps():
    folderToRun = {}

    ############## MLP
    folderInfo = {}
    folderInfo["modelType"]   = "MLP"
    folderInfo["windowSize"]  = "3"
    folderInfo["barrelOnly"]  = "True"
    folderInfo["useLayers"]   = "True"
    folderInfo["endcapOnly"]  = ""
    folderInfo["cylindrical"] = "" 
    folderInfo["retrain"]     = ""
    folderToRun["MLP_ws3_barrel_layerInfo"] = folderInfo

    # folderInfo = {}
    # folderInfo["modelType"]         = "MLP"
    # folderInfo["windowSize"]        = "3"
    # folderInfo["barrelOnly"]  = "True"
    # folderInfo["useLayers"]   = "True"
    # folderInfo["endcapOnly"]  = ""
    # folderInfo["cylindrical"] = "" 
    # folderInfo["retrain"]     = ""
    # folderToRun["MLP_ws3_barrel"] = folderInfo

    # folderInfo = {}
    # folderInfo["modelType"]         = "RNN"
    # folderInfo["windowSize"]        = "3"
    # folderInfo["barrelOnly"]  = "True"
    # folderInfo["useLayers"]   = "True"
    # folderInfo["endcapOnly"]  = ""
    # folderInfo["cylindrical"] = "" 
    # folderInfo["retrain"]     = ""
    # folderToRun["RNN_ws3_barrel_layerInfo"] = folderInfo

    # folderInfo = {}
    # folderInfo["modelType"]         = "biRNN"
    # folderInfo["windowSize"]        = "3"
    # folderInfo["barrelOnly"]  = "True"
    # folderInfo["useLayers"]   = "True"
    # folderInfo["endcapOnly"]  = ""
    # folderInfo["cylindrical"] = "" 
    # folderInfo["retrain"]     = ""
    # folderToRun["biRNN_ws3_barrel_layerInfo"] = folderInfo

    return folderToRun