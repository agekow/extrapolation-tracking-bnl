import os
#################################################
##              EvalHTT                       ##
#################################################
Eval = []


baseDataPath = ""
# baseDataPath = "/data/data/"

#folderToRun = {}


#####################################
# Eval
#####################################
from ListWindowTraining import ACTS_training as ACTS
folderToRun = ACTS.getMaps()

#####################################
# Run Settings
#####################################

from ListWindowTraining import runSettings as RS
runTypes = RS.getACTSRunSettings()
for key in folderToRun.keys():
    baseName = key
    print key
    for runSetting in runTypes:
        counter = 0
        
        inputDict = folderToRun[key]
        inputDict["baseName"]   = key
        inputDict["name"]       = "J" + str(counter)
        inputDict["type"]       = "windowTraining"
        inputDict["dataPath"]  = "/usatlas/u/agekow/usatlasdata/alexTrackNNAnalysis/run/PickleOutput/processed/pickles/test.root_0.pkl"

        # copy from runSettings
        keyList = ["epochs", "batch", "retrain"]
        for dictKey in keyList:
            if(dictKey in runSetting):
                inputDict[dictKey] = ''.join(runSetting[dictKey].split())

        for dictKey in keyList:
            if(dictKey in folderToRun[key]):
                inputDict[dictKey] = ''.join(folderToRun[key][dictKey].split())

        Eval.append(inputDict)
        counter += 1


# inputDict = {}
# inputDict["baseName"]   = "WindowTraining" + key
# inputDict["name"]       = "J" + str(counter)
# inputDict["type"]       = "windowTraining"
# inputDict["dataPath"]  = '/usatlas/u/agekow/usatlasdata/alexTrackNNAnalysis/run/PickleOutput/processed/pickles/test.root_0.pkl'
# inputDict["epochs"]     = "2000"
# inputDict["batch"]      = "1024"
# inputDict["windowSize"] = "3"
# inputDict["modelType"]  = "MLP"
# inputDict["cylindrical"] = "" 
# inputDict["retrain"]     = ""
# inputDict["barrelOnly"]  = "True"
# inputDict["useLayers"]   = "True"
# inputDict["endcapOnly"]  = ""


