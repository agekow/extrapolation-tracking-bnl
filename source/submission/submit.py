import os, sys
import argparse
from jobSubmission import *

parser = argparse.ArgumentParser()
parser.add_argument("--queue",                      default = "workday", type = str,      help = "Queue to submit to.")
parser.add_argument("--dryRun",                     default = False, action='store_true',   help = "Does not submit to anything") 
parser.add_argument("--runLocal",                   default = False, action='store_true',   help = "Run in parallel") 
parser.add_argument("--runInteractive",             default = False, action='store_true',   help = "Run in interactive move") 
parser.add_argument("--submitCondor",               default = False, action='store_true',   help = "Submit jobs to the batch") 
parser.add_argument("--submitSlurm",                default = False, action='store_true',   help = "Submit jobs to the batch") 
parser.add_argument("--submitCedarContainer",       default = False, action='store_true',   help = "submission for cedarContainer")
parser.add_argument("--cedarTime",                  default = "6:00:00", type = str,        help = "Time for a job to run on Cedar. Needs to be in format hh:mm:ss")
parser.add_argument("--maxProcs",                   default = 4,     type=int,              help = "Number of parallel processed")

parser.add_argument("--remergeResplit",             default = False, action='store_true',   help = "expert option to merge and split jobs")
parser.add_argument("--remergeResplitSize",         default = 1,     type = int,            help = "expert option to find how many to merge jobs with")

parser.add_argument("--windowTraining",                   default = False, action='store_true',   help = "traing network using sliding window of hits") 

args = parser.parse_args()


submissionJobList = []

if args.windowTraining:
    import jobListWindowTraining
    submissionJobList += [jobListWindowTraining.Eval]

def main():
    submissionManager = submissionFactory(args)
    index = 0

    ##############################################################
    ##                     Configure the jobs                   ##
    ##############################################################
    for submissionJob in submissionJobList:
        for job in submissionJob:

            bsubBaseDir = "bsub/"
            if(args.submitCondor): bsubBaseDir = "condor/" 
            elif(args.submitSlurm or args.submitCedarContainer):   bsubBaseDir     = "slurm/" 

            bsubBaseDir += "/" + job['baseName']
            os.system("mkdir -vp " + bsubBaseDir)

            cmd = ""
            if(job["type"] == "windowTraining"):
                index += 1 
                exePath = "python /usatlas/u/agekow/usatlasdata/trackExtrapolation_training/source/windowTraining.py"
                cmd = _getWindowTrainingCommand(exePath, job) 
            else: 
                print("Type not recognized, ", job["type"] )
                exit(1)

            cInfo = {}
            cInfo["jobOutPath"] = job["baseName"]
            cInfo["jobOutName"] = job['name']

            os.system("mkdir -p jobList/%s" % cInfo["jobOutPath"])
            _writeJobList(cInfo["jobOutPath"], cInfo["jobOutName"], cmd);

            submissionManager.addJob(cInfo)


    ##############################################################
    ##                      Submit the jobs                     ##
    ##############################################################
    ## This is the actual submission to different systems, e.g. condor, batch, grid, local
    ## Information from the jobManager is re-organized here 
    submissionManager.process()
    submissionManager.submitJob()

def _writeJobList(jobOutPath, jobOutName, cmd):
    fileName = "jobList/" + jobOutPath + "/" + jobOutName + ".sh"
    fileObj = open(fileName, 'w')
    fileObj.write("#!/bin/bash\n\n")
    if(not (args.runLocal or args.runInteractive)): 
        CWD = os.getcwd()
        fileObj.write("cd " + CWD + "\n")
        fileObj.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase " + "\n")
        fileObj.write("source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh" + "\n")
        fileObj.write("lsetup \"ROOT 6.22.00-python3-x86_64-centos7-gcc8-opt\"" + "\n")
        fileObj.write("source /usatlas/u/agekow/virtual_envs/myenv-3.7.6/bin/activate + \n")
    fileObj.write(cmd)
    fileObj.write("\n")
    fileObj.close()

    os.system("chmod u+x " + fileName)

def submissionFactory(args):
    if(args.submitCondor):           return submitCondor(args)
    elif(args.submitCedarContainer): return submitCedarContainer(args)
    elif(args.runLocal):             return runLocal(args)
    elif(args.runInteractive):       return runInteractive(args)
    elif(args.dryRun):               return doDryRun(args)
    else:
        print("You did not enter where to submit to")
        print("-------------------------------------------------")
        exit(1)

def _getWindowTrainingCommand(exePath, currJob):
    cmd =  exePath + " "
    cmd += ' --dataPath %s' %(currJob['dataPath'])
    cmd += ' --epochs %s' %(currJob['epochs'])
    cmd += ' --batch %s' %(currJob['batch'])
    cmd += ' --windowSize %s' %(currJob['windowSize'])
    cmd += ' --modelType %s' %(currJob['modelType'])

    if currJob["cylindrical"]: cmd += ' --cylindrical'
    if currJob["retrain"]:      cmd += ' --retrain'
    if currJob["barrelOnly"]:   cmd += ' --barrelOnly'
    if currJob["useLayers"]:    cmd += ' --useLayers'
    if currJob["endcapOnly"]: cmd += ' --endcapOnly'

    return cmd

if __name__ == '__main__':
  sys.exit( main() )
