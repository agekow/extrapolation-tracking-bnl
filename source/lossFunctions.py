from commonFunctions import rmNumpyPath
rmNumpyPath()

import tensorflow as tf
import numpy as np
# import tensorflow_probability as tfp

def cosineSimilarity(y_true, y_pred):
    norm_true = tf.norm(y_true)
    norm_pred = tf.norm(y_pred)
    norm = norm_true * norm_pred
    return 1 - tf.tensordot(y_true, y_pred,2) / norm

def penalizeRho(y_true, y_pred):
    #predictions in the form (x,y,z)
    zError = tf.reduce_mean(tf.math.square(y_true[:,2]-y_pred[:,2]), axis=-1)
    rhoError = tf.reduce_mean(abs(tf.math.square(y_true[:,0]) + tf.math.square(y_true[:,1]) - (tf.math.square(y_pred[:,0]) + tf.math.square(y_pred[:,1]))))
    #return mse + tf.math.exp(rhoError)
    return zError + 10*rhoError

def penalizeZ(y_true, y_pred):
    mse = tf.reduce_mean(tf.math.square(y_true-y_pred), axis=-1)
    z = tf.reduce_mean(tf.math.square(y_true[:,2] - y_pred[:,2]), axis=-1)
    return mse+10*z

def angleMSE(y_true, y_pred):
    mse = tf.reduce_mean(tf.math.square(y_true-y_pred), axis=-1)
    return  cosineSimilarity(y_true, y_pred) + mse

def mseNorm(y_true, y_pred):
    # mse, but scaled by the true value. Interpreted as % error so that inner predictions penalized as high as outer ones
    se = tf.math.divide(y_true-y_pred, tf.norm(y_true))
    #se = tf.math.multiply(se, tf.divide(1, tf.norm(y_true) )
    se = tf.math.square(se)
    loss = tf.reduce_mean(se, axis=-1)
    return loss

def paddedMSE(y_true, y_pred):
    mask = tf.math.logical_not(tf.math.equal(y_true[:,0], 0)) #if one component of y_true is 0, they all should be
    loss_ = tf.keras.losses.MeanSquaredError(reduction='none')
    loss = loss(y_true, y_pred)
    mask = tf.cast(mask, dtype=loss_.dtype)
    loss_ *= mask

def gaus_llh_loss(outputs, targets):
    """
    Custom gaussian log-likelihood loss function
    To be used with Gaussian RNN
    """
    means = outputs[0]
    covs = outputs[1]
    #print(len(outputs))
    #print("mean shape: ", means.shape, " covs shape: ", covs.shape)
    m_shape = tf.shape(means)
    t_shape = tf.shape(targets)
    c_shape = tf.shape(covs)
    means = tf.reshape(means, (m_shape[0]*m_shape[1], m_shape[2]))
    targets = tf.reshape(targets,(t_shape[0]*t_shape[1], t_shape[2]))
    covs = tf.reshape(covs, (c_shape[0]*c_shape[1], c_shape[2], c_shape[3]))
    # Calculate the inverses of the covariance matrices
    inv_covs = tf.linalg.inv(covs)
    # Calculate the residual error
    res = targets - means
    #res = tf.reshape(res, (shape[0], shape[1],2,1))
    # Calculate the residual error term
    res_right = tf.matmul(inv_covs, res[:,:,None])
    res_term = tf.matmul(res[:,None,:], res_right)
    diag_chols = tf.linalg.diag_part(tf.linalg.cholesky(covs))
    log_det = tf.reduce_sum(diag_chols, axis=1)
    log_det = tf.math.log(log_det)*2
    gllh_loss = tf.reduce_sum(res_term[:,:,0] + log_det, axis=-1)
    
    return tf.reduce_mean(gllh_loss)

def detectorClassification(outputs, targets):
    loss = {"volume": "categorical_cross_entropy", "layer":"categorical_cross_entropy"}
    return loss