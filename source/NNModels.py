import tensorflow as tf
from tensorflow.keras.layers import Dense, Flatten, GRU, Input, Bidirectional
from tensorflow.keras.models import Sequential
#from qkeras import QDense, QActivation
#import tensorflow_addons as tfa

def mlp(window_size=3, n_features=3):
    #window_size refers to the number of hits included in a data sample used for prediction
    #n_feautres refers to the number of features per hit, ex. 3 for x,y,z
    model  = Sequential([
        Flatten(), 
        Dense(128, activation='relu'),
        Dense(256, activation='relu'),
        #Dense(128, activation='relu'),
        Dense(32, activation='relu'),
        Dense(n_features, activation='tanh')
    ])
    return model

def detectorClassifier(window_size=3, n_features=3):
    #window_size refers to the number of hits included in a data sample used for prediction
    #n_feautres refers to the number of features per hit, ex. 3 for x,y,z
    inputs = Input(shape=(window_size,n_inputFeatures))
    x = Dense(128, activation='relu')(inputs)
    x = Dense(128, activation='relu')(x)
    layerClass = Dense(30, activation='softmax')(x) #30 possible layers
    volumeClass = Dense(15, activation='softmax')(x) #15 volumes in ITK
    outputs = tf.keras.layers.Concatenate([volumeClass, layerClass])
    return tf.keras.Model(inputs=inputs, outputs=outputs)
    
def rnn(window_size=3, n_inputFeatures=20,n_outputFeatures=3):
    inputs = Input(shape=(window_size,n_inputFeatures))
    #x = Masking(mask_value=0)(inputs)
    x = GRU(32, return_sequences=True)(inputs)
    #x = LSTM(32, return_sequences=True)(x)
    #x = LayerNormalization()(x)
    x = GRU(32, return_sequences=False)(x)
    #x = LayerNormalization()(x)
    #x = Dense(64, activation='selu')(x)
    outputs = Dense(n_outputFeatures, activation='tanh', name='mu')(x)
    #outputs = Dense(3, activation='tanh', name='mu')(x)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    return model

def biRNN(window_size=3, n_inputFeatures=48,n_outputFeatures=3):
    inputs = Input(shape=(window_size,n_inputFeatures))
    #x = Masking(mask_value=0)(inputs)
    x = Bidirectional(GRU(64, return_sequences=True))(inputs)
    #x = LSTM(32, return_sequences=True)(x)
    #x = LayerNormalization()(x)
    x = Bidirectional(GRU(64, return_sequences=False))(x)
    #x = LayerNormalization()(x)
    x = Dense(64, activation='selu')(x)
    outputs = Dense(n_outputFeatures, activation='tanh', name='mu')(x)
    #outputs = Dense(3, activation='tanh', name='mu')(x)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    return model

def rnnFullSequence(n_inputFeatures=3, n_outputFeatures=3):
    """
    input data must be carefully padded. The input sequence length will be equal to the output sequence length
    Masking layer is buggy in ONNX
    """
    model = tf.keras.models.Sequential([
        #tf.keras.layers.Masking(),
        GRU(32, return_sequences=True),
        GRU(32, return_sequences=True),
        GRU(3, return_sequences=True)
    ])
    return model

class GaussianRNN(tf.keras.Model):
    """
    To be trained using gaus_llh_loss.
    Requires its own training script due to the custom training loop
    """
    def __init__(self, n_inputHits, n_inputFeatures=3, n_outputFeatures=5):
        #output feature_1, feature_2, sigma_1, sigma_2, corr_12
        super().__init__()
        self.n_inputHits = n_inputHits
        
        self.inputs = Input(shape=(n_inputHits,n_inputFeatures))
        self.gru1 = GRU(32, return_sequences=True)
        self.gru2 = GRU(32, return_sequences=True)
        self.out = Dense(5)
        
    def call(self, inputs):
        shape = tf.shape(inputs)
        x = self.gru1(inputs)
        x = self.gru2(x)
        outputs = self.out(x)
        outputs = tf.reshape(outputs, (-1, 5))
        mean = outputs[:,:2]
        var = tf.math.exp(outputs[:,2:4])
        var = tf.math.sqrt(var)
        correlations = tf.math.tanh(outputs[:,4:])
        correlations = tf.reshape(correlations, (shape[0]*shape[1],1,1))
        identity = tf.eye(2, batch_shape=[shape[0]*shape[1]],dtype=tf.float32)
        
        corr_matrix = tf.cast(tf.math.equal(identity,0), tf.float32) * correlations
        corr_matrix = identity+corr_matrix
        
        covs = tf.matmul(var[:, :,None], var[:, None, :])
        covs = covs * corr_matrix
        mean = tf.reshape(mean, (shape[0], shape[1], 2))
        covs = tf.reshape(covs, (shape[0], shape[1],2,2))
        return mean, covs
    
    def train_step(self, inputs):
        inputs, targets = inputs
        with tf.GradientTape() as tape:
            outputs = self(inputs)
            loss = self.compiled_loss(outputs, targets)

        grads = tape.gradient(loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        return {m.name: m.result() for m in self.metrics}

"""
Requires tensorflow_addons and tf >= 2.8
If we cannot get this to work, we can define an ESN ourselves
"""
# def esn(window_size=3, n_inputFeatures=3, n_outputFeatures=3):
#     model = tf.keras.models.Sequential()
#     model.add(tf.keras.layers.InputLayer(input_shape=(window_size,n_inputFeatures)))
#     model.add(tfa.layers.ESN(200, connectivity=.2))
#     model.add(tf.keras.layers.Dense(n_outputFeatures, activation='tanh'))
#     return model


def qmlp(window_size=3, n_features=3, bits=8):

    input_layer = tf.keras.Input(shape=(window_size,n_features))

    x = tf.keras.layers.Flatten()(input_layer)
    x = QDense(64,
               kernel_quantizer = 'quantized_bits({},0,alpha=1)'.format(bits),
               bias_quantizer = 'quantized_bits({},0,alpha=1)'.format(bits))(x)
    x = QActivation('quantized_relu({},0)'.format(bits))(x)
    x = QDense(32,
               kernel_quantizer = 'quantized_bits({},0,alpha=1)'.format(bits),
               bias_quantizer = 'quantized_bits({},0,alpha=1)'.format(bits))(x)
    x = QActivation('quantized_relu({},0)'.format(bits))(x)
    x = QDense(1,
               kernel_quantizer = 'quantized_bits({},0,alpha=1)'.format(bits),
               bias_quantizer = 'quantized_bits({},0,alpha=1)'.format(bits))(x)

    output = tf.keras.layers.Activation('tanh')(x)

    model = tf.keras.Model(inputs=[input_layer], outputs=[output])

    return model

