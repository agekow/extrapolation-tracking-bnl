from commonFunctions import rmNumpyPath
rmNumpyPath()
import sys
import pickle 
import tensorflow as tf
import numpy as np
import os

#os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
import matplotlib.pyplot as plt
from NNModels import *
from sklearn.model_selection import train_test_split
import argparse 
import lossFunctions, learningRates
from tracksDataset import *
#import tensorflow_addons as tfa
#import qkeras

tf.keras.backend.clear_session()

parser = argparse.ArgumentParser()
parser.add_argument("--dataPath", type=str, default='/usatlas/u/agekow/trackingPickles/formatted/', help="file path to pickle data")
parser.add_argument("--windowSize", type=int, default=3, help="window size of algorithm")
parser.add_argument("--epochs", type=int, default=100, help="number of epochs to train for")
parser.add_argument("--batch", type=int, default=2048, help="batch size")
parser.add_argument("--modelType", type=str, default="MLP",
                    const="MLP",
                    nargs="?",
                    choices=["MLP", "RNN","biRNN", "ESN"], help="choose from MLP, RNN, biRNN, ESN")
#subparsers = parser.add_subparsers()
#bits = subparsers.add_parser('bits', help='number off bits for quantized network')
#bits.add_argument("--bits", type=int, default=8, help="Number of bits if using quantized network")
#parser.add_argument("--modelName", type=str, required=True, help="Name of saved model")
parser.add_argument("--cylindrical", action='store_true', help="use cylindrical coordinates")
parser.add_argument("--retrain", action='store_true', help="use cylindrical coordinates")
parser.add_argument("--useLayers", action='store_true', default=False, help="use one hot encoded layer indices during training") #default can be reset to false
parser.add_argument("--barrelOnly", action='store_true', default=False, help="use only track segments in the barrel")
parser.add_argument("--endcapOnly", action='store_true', default=False, help="use only track segments in the endcap")
args = parser.parse_args()
### If using Pre-formatted pickles ###    
trainingData = np.load(args.dataPath)
X = trainingData["inputs"]
Y = trainingData["targets"]
if not args.useLayers: X = X[:,:,0:3]
n_inputFeatures = X.shape[2]
n_outputFeatures = Y.shape[1]
print("n_input features: ", n_inputFeatures, " n_output features: ", n_outputFeatures)

x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size=.15)
print("number of training samples: ", len(x_train))
print("number of validation samples: ", len(x_test))

if args.barrelOnly: detectorRegion = '_barrel'
elif args.endcapOnly: detectorRegion = '_ec'
else: detectorRegion=''

if args.useLayers: layers = '_layerInfo'
else: layers=''

if args.cylindrical: cyl="_cyl"
else: cyl=""

modelName = 'Extrap_'+args.modelType + '_ITK_ws' + str(args.windowSize) + cyl+ detectorRegion +layers
print(modelName)

if args.retrain:
    model = tf.keras.models.load_model('trained_models/{}'.format(modelName))
    print("loaded pre-trained model")

else:
    if args.modelType == 'RNN':
        model = rnn(args.windowSize, n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'MLP':
        model = mlp(args.windowSize, n_outputFeatures)
    elif args.modelType == "biRNN":
        model = biRNN(args.windowSize, n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'ESN':
        model = esn(args.windowSize, n_inputFeatures, n_outputFeatures)
    #elif args.modelType == 'qMLP':
    #    model = qMLP.qmlp(window_size=args.windowSize, n_features=n_features, bits=args.bits)
    
    
checkpoint_filepath = 'checkpoints/' + modelName
if os.path.exists(checkpoint_filepath): model.load_weights(checkpoint_filepath)
else: os.system("mkdir -vp "+checkpoint_filepath)
callbacks = [tf.keras.callbacks.ModelCheckpoint(checkpoint_filepath, monitor='val_loss', verbose=0, save_best_only=True, save_weights_only=True),
			tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=70)]

#utilize more data by training over several training files
training_history = {"loss":[], "val_loss":[]}

warmup_steps = x_train.shape[0] // args.batch * args.epochs // 5 # 1/5 of the training will be warmup steps
learning_rate = learningRates.CustomSchedule(32, warmup_steps)
#learning_rate = .001
optimizer = tf.keras.optimizers.Adam(learning_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)

loss = 'mse' 
model.compile(optimizer=optimizer, loss=loss)
history = model.fit(x_train, y_train, epochs=args.epochs, batch_size=args.batch, validation_data=(x_test, y_test), callbacks=callbacks)
training_history['loss'] += history.history['loss']
training_history["val_loss"] += history.history['val_loss']

model.load_weights(checkpoint_filepath)
# save the best model only
from shutil import rmtree
os.system("mkdir -vp trained_models/")
modelPath = "trained_models/" +modelName
if os.path.exists(modelPath): rmtree(modelPath)
tf.keras.models.save_model(model, modelPath, include_optimizer=False)

#save training info to plot later
os.system("mkdir -vp loss_info/")
with open("loss_info/"+modelName + ".pkl", "wb") as f:
    pickle.dump(training_history, f)


# plt.plot(range(len(history.history['loss'])), history.history['loss'], label='train')
# plt.plot(range(len(history.history['val_loss'])), history.history['val_loss'], label='train')
# plt.xlabel('epochs')
# plt.ylabel('loss')
# plt.savefig('trained_models/{}_loss'.format(modelName))
