# Training Neural Networks for Track Extrapolation
# First Time Setup
```
mkdir extrapolationTracking
cd extrapolationTracking
mkdir run source
gitlab clone https://gitlab.cern.ch/agekow/extrapolation-tracking-bnl.git source
```
## Data Preperation
data is already prepared here: `/hpcgpfs01/scratch/sabidi/ExtrapolationPickles/SingleMu_ttbar_ttbarMu40_s0-30.npz`
More can be prepared by running the script trackDataset. The file above contains 16 gb of track data from SingleMu, ttbarMu0, and ttbarMu40 samples. The tracks are split into input and target data. The input contains track segments of 3 hits and the target is the corresponding next hit along the track.

## Training A Network
Network definitions can be found, created, and edited in the file `NNModels.py`. If a new architecture is created, it should be added as a command line argument option in the `windowTraining.py` script. 

Models should be trained via the `icsubmit01` node, otherwise the training data will not be accessible.

To train a model, run `windowTraining.py` from the `run` directory.
Example command: `python ../source/windowTraining.py --dataPath /hpcgpfs01/scratch/sabidi/ExtrapolationPickles/SingleMu_ttbar_ttbarMu40_s0-30.npz --modelType MLP --epochs 500 --batch 5096 --barrelOnly --useLayers`

The model will be saved in the `run/trained_models` directory.

Convert it to onnx. `python -m tf2onnx.convert --saved-model trained_models/Extrap_MLP_ACTS_ws3_barrel_layerInfo/ --output onnx Extrap_MLP_ACTS_ws3_barrel_layerInfo.onnx`